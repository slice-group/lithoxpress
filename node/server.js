//                                             SERVER.JS
//-------------------------------------------------------------------------------------------------------------

var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var redis = require("redis");
var sesiones = require('./chat_module/sesiones');  
var clients = {};
var admin = {};


server.listen(3002);
io.sockets.on('connection', function (socket) {  

  //conectar administrador
  socket.on('conectar_admin', function(data){
    sesiones.connect_admin(this, data, admin)
    io.sockets.emit('button', true)//envia la emisión cuandos e conecta un administrador
    console.log(admin)
  });

  //conectar cliente
  socket.on('conectar_cliente', function(data){
    sesiones.connect_client(this, data, clients)
    io.sockets.emit('show_chat', data)//envia emisión de chat al administrador
    console.log(clients)
  });   

  //habilitar buton
  if (sesiones.hash_blank(admin) != 0) {
    io.sockets.emit('button', true)
  }

  //habilitar boton de chat
  socket.on('enable-support', function(data){
    if (sesiones.hash_blank(admin) != 0) {
      io.sockets.emit('button', true)
    }
  });

  socket.on('atendiendo_chat', function(data) {
    clients[data.client_id][0].client.status = 1
    sesiones.send_all_admins(admin, io, "atendido", { client: clients[data.client_id][0].client })
  });

  //enviar msg del chat
  socket.on('send_message', function(data){
    sesiones.send_message(data, io, admin, clients, socket);
  });

  //mostrar todos los clientes en el index
  socket.on("all_clients", function(data){
    sesiones.list_clients(data.id, clients, admin, io)
  });

  //metodos que se ejecutan al desconectarse un socket
  socket.on('disconnect', function () {
    sesiones.disconnect_admin(this, admin, clients, io);
    sesiones.disconnect_client(this, clients, io);

    if (sesiones.hash_blank(admin) == 0) {
      io.sockets.emit('button', false)
    }
  });

});
