Rails.application.routes.draw do
  require 'sidekiq/web'
  require 'sidetiq/web'

  mount Sidekiq::Web => '/sidekiq'

  resources :validations, :only => ['new', 'update', 'edit']
  post 'validations/:id' => 'validations#create', :as => 'create_validation'
  get 'catalog/stamps/:id' => 'visitors#catalog', :as => 'catalog'
  get 'catalog/modal/:stamp_id' => 'visitors#modal', :as => 'catalog_modal'
  get '/orders/update_terms' => 'orders#update_terms'
  post '/orders/create' => 'orders#create'
  post '/create' => 'orders#create'

  resources :attachments, :only => ['create', 'update', 'edit', 'destroy']

  root to: 'admin#index'

  devise_for :users, skip: [:registration]

  resources :admin, only: [:index]

  scope :admin do
  	resources :users do
  		collection do
  			get '/angular_index', to: 'users#angular_index'
	  		post '/delete', to: 'users#delete'
  		end
  	end
  end

  scope :users do
    post '/registration', to: 'users#register', as: "register_user"
    get '/sign_up', to: 'users#sign_up', as: "sign_up_user"
  end

  resources :ods do
    collection do
      get '/angular_index', to: 'ods#angular_index'
      get ':id/clone', to: 'ods#clone', as: 'clone'
    end
  end

  resources :reorders, only: [:update, :edit, :destroy, :index, :show] do 
    collection do
      get '/angular_index', to: 'reorders#angular_index'
      post ':id/changed_status/:stat', to: 'reorders#changed_status', as: 'changed_status'
      post '/delete', to: 'reorders#delete'
    end
  end

  resources :orders, only: [:new, :create]

  scope :admin do
    resources :stamps do
      collection do
        get '/angular_index', to: 'stamps#angular_index'
        post '/delete', to: 'stamps#delete'
      end
    end

    resources :categories do 
      collection do
        get '/angular_index', to: 'categories#angular_index'
        post '/delete', to: 'categories#delete'
      end
    end
  end

  scope :admin do
    resources :miscellaneous, :only => ['update', 'edit'] do
      post '/send_notifications_manual', to: 'miscellaneous#send_notifications_manual'
    end
  end

end
