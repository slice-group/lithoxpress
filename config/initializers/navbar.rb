require "#{Rails.root}/lib/engine_navbar.rb"

Navbar.setup do |config|
	
  config.routes_admin={}
  config.routes_front={}

  Rails::Engine.subclasses.each do |engine| 
  	if engine.parent.constants.include? :SITEBAR
  		config.routes_admin.merge!(engine.parent::SITEBAR)
  	end
  end
  
end