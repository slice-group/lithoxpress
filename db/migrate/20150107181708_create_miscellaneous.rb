class CreateMiscellaneous < ActiveRecord::Migration
  def change
    create_table :miscellaneous do |t|
      t.string :email_design
      t.string :email_ventas

      t.timestamps
    end
  end
end
