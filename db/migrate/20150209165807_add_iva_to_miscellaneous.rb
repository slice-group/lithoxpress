class AddIvaToMiscellaneous < ActiveRecord::Migration
  def change
    add_column :miscellaneous, :iva, :float
  end
end
