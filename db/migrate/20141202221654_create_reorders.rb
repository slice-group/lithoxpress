class CreateReorders < ActiveRecord::Migration
  def change
    create_table :reorders do |t|
      t.string :ods_id
      t.string :status
      t.float :cantidad

      t.timestamps
    end
  end
end
