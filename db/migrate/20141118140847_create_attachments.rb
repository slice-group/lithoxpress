class CreateAttachments < ActiveRecord::Migration
  def change
    create_table :attachments do |t|
      t.string :ods_id
      t.string :image

      t.timestamps
    end
  end
end
