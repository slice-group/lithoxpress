class AddColumnsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :phone, :string
    add_column :users, :rif, :string
  end
end
