class AddSelectionImageToValidations < ActiveRecord::Migration
  def change
    add_column :validations, :selection_image, :string
  end
end
