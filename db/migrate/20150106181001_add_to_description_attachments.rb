class AddToDescriptionAttachments < ActiveRecord::Migration
  def change
  	add_column :attachments, :description, :text
  	add_column :attachments, :choose_model, :boolean
  end
end
