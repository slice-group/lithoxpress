class AddPriceToReorders < ActiveRecord::Migration
  def change
    add_column :reorders, :price, :float
  end
end
