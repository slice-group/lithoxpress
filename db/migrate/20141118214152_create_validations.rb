class CreateValidations < ActiveRecord::Migration
  def change
    create_table :validations do |t|
      t.string :status
      t.text :description
      t.boolean :active
      t.string :ods_id

      t.timestamps
    end
  end
end
