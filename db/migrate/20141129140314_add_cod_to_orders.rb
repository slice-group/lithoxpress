class AddCodToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :cod, :string
  end
end
