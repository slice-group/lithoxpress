class AddNotificationTimeToMiscellaneous < ActiveRecord::Migration
  def change
    add_column :miscellaneous, :notification_time, :string
  end
end
