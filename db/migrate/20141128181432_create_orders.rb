class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :type
      t.string :format
      t.string :ink
      t.string :material
      t.string :copies
      t.integer :amount

      t.timestamps
    end
  end
end
