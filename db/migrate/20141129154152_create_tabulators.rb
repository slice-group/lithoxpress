class CreateTabulators < ActiveRecord::Migration
  def change
    create_table :tabulators do |t|
      t.string :cod
      t.text :description
      t.float :price

      t.timestamps
    end
  end
end
