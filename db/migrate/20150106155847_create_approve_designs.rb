class CreateApproveDesigns < ActiveRecord::Migration
  def change
    create_table :approve_designs do |t|
      t.boolean :approve
      t.string :ods_id

      t.timestamps
    end
  end
end
