class AddDescriptionToReorders < ActiveRecord::Migration
  def change
    add_column :reorders, :description, :text
  end
end
