class CreateComplementStamps < ActiveRecord::Migration
  def change
    create_table :complement_stamps do |t|
      t.string :image_stamp
      t.string :example_stamp
      t.string :height
      t.string :width
      t.boolean :public
      t.string :stamp_id
      t.integer :category_id
      t.integer :group
      t.string :almohadilla_id
      t.string :caja_id
      t.string :goma_id

      t.timestamps
    end
  end
end
