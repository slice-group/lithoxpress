class AddColumnsToMiscellaneous < ActiveRecord::Migration
  def change
    add_column :miscellaneous, :phone_support, :string
    add_column :miscellaneous, :email_support, :string
  end
end
