# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#user = CreateAdminService.new.call
#puts 'CREATED ADMIN USER: ' << user.email

[:admin, :ventas, :diseñador, :cliente, :suspendido].each do |name|
	Role.create name: name
end

[:pendiente, :diseñado, :impreso, :numerado, :compaginado, :armada, :empaquetado, :entregado].each do |name|
	Stat.create name: name
end


User.create name: "Admin", email: "admin@inyxtech.com", password: "12345678", password_confirmation: "12345678", role_ids: "1"

Miscellaneou.create(email_design: "diseño@suiemprenta.com.ve", email_ventas: "ventas@suiemprenta.com.ve", iva: 12.00, phone_support: "12345678", email_support: "soporte@suimprenta.com.ve", notification_time: "10:16")