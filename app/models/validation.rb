class Validation < ActiveRecord::Base
	before_save :add_selection_image
	belongs_to :ods
	validates_presence_of :status, :description, :ods_id
	#validate :slection_image_validate

	def self.group_by_status
	    rtrn = {}
	    ["Aprobado", "Desaprobado", "Aprobado con observaciones"].each do |rol|
	      rtrn[rol] = Ods.status_design_count(rol)
	    end
	    rtrn
  end

  def add_selection_image
  	unless Validation.where(ods_id: self.ods_id).blank?
	  	if self.status != "Desaprobado" and !Validation.where(ods_id: self.ods_id).last.selection_image.blank?
	  		self.selection_image = Validation.where(ods_id: self.ods_id).last.selection_image 
	  	end
	  end
  end

  def self.select_image_permit?(id)
  	order = Ods.find(id)

  	if order.validations.blank?
  		true
  	elsif order.validations.last.status == "Desaprobado"
  		true
  	else
  		false
  	end	
  end

  def self.status_options(id)
  	if select_image_permit?(id)
  		['Aprobado', "Aprobado con observaciones", "Desaprobado"]
  	else
  		['Aprobado', "Aprobado con observaciones"]
  	end
  end

  def colors_announce
  	if self.status == "Desaprobado"
  		"red"
  	elsif self.status == "Aprobado con observaciones"
  		"#5bc0de"
  	else
  		"green"  		
  	end
  end


  #validacion para selecion de imagenes
  def slection_image_validate
  	order = Ods.find(self.ods_id)

  	if order.approve_design.approve and self.status != "Desaprobado" and self.selection_image.blank?
    	errors.add(:selection_image, 'La selección de imagen no puede estar en blanco.')
    end
  end

end
