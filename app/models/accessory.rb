class Accessory < ActiveRecord::Base
	establish_connection :lithoxpress_database_development
	self.table_name = "accesories"
  self.primary_key = "codigo"

	def as_json(options={})
    {
      codigo: self.codigo.strip,
      nombre: self.nombre.strip,
      precio: self.precio1.round(2),
      precioiva: self.preciofin1.round(2)
    }
	end

  def complement_accessory
    ComplementAccessory.find_by_accessory_id self.codigo.strip
  end

end