class Attachment < ActiveRecord::Base
  belongs_to :ods
  mount_uploader :image, ImageUploader

  validates_presence_of :image
  validate :picture_size_validation, :if => "image?"

  def picture_size_validation
   	errors[:image] << "Imagén no puede pesar más de 1 MB" if self.image.size > 1.megabytes
	end

	def name
    self.image.file.original_filename.split(".").first.gsub("_", " ")
  end

end
