class Reorder < ActiveRecord::Base
	belongs_to :user
	belongs_to :ods

	validates_presence_of :cantidad, :price, :emision_date
	validates_numericality_of :price, greater_than_or_equal_to: 0
	validates_numericality_of :emision_date, greater_than_or_equal_to: 0

	def self.por_aprobar
		Reorder.where(["status = ?", "Por aprobar"]).count
	end

	def as_json(options={})
		{
			id: self.id,
			client_name: Cliempre.find_by_codigo(Ods.find(self.ods_id).Cliente).nombre,
			ods_id: self.ods_id,
			ods: self.ods,
			status: self.status,
			cantidad: self.cantidad,
			price: self.price,
			user: self.user.as_json,
			description: self.description.as_json,
			created_at: self.created_at,
			updated_at: self.updated_at
		}
	end
end
