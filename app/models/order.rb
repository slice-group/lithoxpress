class Order < ActiveRecord::Base
	validates_presence_of :type_armed, :format, :ink, :material, :copies
	def create_cod
		inks = { '1 tinta' => 'C1', '2 tintas' => 'C2', '3 tintas' => 'C3', '4 tintas (Full Color)' => 'C4' }
		material = { 'Papel quimico' => 'PQ', 'Papel bond 16' => 'PB16', 'Papel bond 20' => 'PB20' }	
		self.type_armed.first+self.format[2..4]+inks[self.ink]+material[self.material]+self.copies.first
	end
end
