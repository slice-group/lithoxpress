class User < ActiveRecord::Base
  rolify
  validates_presence_of :name, :role_ids
  has_many :orders 
  has_many :reorders
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  

  def self.user_json(current_user)
    json = []
    User.where("id != #{current_user.id}").order('created_at DESC').each do |user|
      u = user.as_json 
      u.merge!({rol: user.roles.first.id })
      json.push u
    end
    json.as_json
  end

  def rol
    self.roles.first.id
  end

  def ods_count
    o = Ods.where('Cliente = "'+self.cod+'"').count
  end

  #dashboard
  def self.group_by_roles
    rtrn = {}
    [:admin, :ventas, :diseñador, :cliente, :suspendido].each do |rol|
      rtrn[rol] = User.with_role(rol).count
    end
    rtrn
  end

  def self.clients_count
      count = 0;
      User.all.each do |user|
        count += 1 if user.has_role? :cliente
      end
      count
  end

  def self.cliempre(email)
    Cliempre.find_by_email(email)
  end

  def self.create_new_user_if_he_have_a_order(date)
    orders = Ods.where(Fecha: Date.parse(date))
    orders.each do |order|
      cliempre = Cliempre.where(codigo: order.Cliente).first
      if !cliempre.email.blank? and User.where(email: cliempre.email).empty?
        puts "Envio el correo a #{cliempre.email}"
        password = SecureRandom.hex(4);
        user = User.new name: cliempre.nombre, email: cliempre.email, password: password, password_confirmation: password, role_ids: "4", cod: cliempre.codigo, rif: cliempre.nrorif, phone: cliempre.telefonos
        if user.save
          UserMailer.mail_registration(User.find_by_email(cliempre.email), password).deliver
        end
      end
    end
  end

  def self.send_notification_if_client_have_a_new_order(date)
    orders = Ods.where(Fecha: Date.parse(date))
    orders.each do |order|
      cliempre = Cliempre.where(codigo: order.Cliente).first
      user = User.find_by_email(cliempre.email)
      if user
        puts "Envio la notificacion a #{cliempre.email}"
        NotificationMailer.ods_new(order, cliempre).deliver
      end
    end
  end

end
