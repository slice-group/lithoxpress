class Stamp < ActiveRecord::Base
	establish_connection :lithoxpress_database_development
	self.table_name = "stamps"
	self.primary_key = "codigo"

	def as_json(options={})
    {
      codigo: self.codigo.strip,
      nombre: self.nombre.strip,
      precio: self.precio1.round(2),
      category: self.complement_stamp ? self.complement_stamp.category.name : "Sin categoría",
      group: self.complement_stamp ? "G#{self.complement_stamp.group}" : "N/A",
      precioiva: self.preciofin1.round(2)
    }
	end

	def self.all_stamps
		Stamp.where(grupo: 1, subgrupo: 6)
	end

	def complement_stamp
		ComplementStamp.find_by_stamp_id(self.codigo)
	end
end
