class Category < ActiveRecord::Base
	has_many :complement_stamps

	def as_json(options={})
    {
      id: self.id,
      name: self.name,
      description: self.description
    }
	end

end
