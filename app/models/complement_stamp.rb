class ComplementStamp < ActiveRecord::Base
	mount_uploader :image_stamp, ImageUploader
	mount_uploader :example_stamp, ImageUploader
	has_many :parts
	belongs_to :category
	accepts_nested_attributes_for :parts, :allow_destroy => true

	validates_presence_of :height, :width, :category_id, :group

	def stamp
		Stamp.find_by_codigo self.stamp_id
	end

	def almohadilla
		Accessory.find self.almohadilla_id
	end

	def goma
		Accessory.find self.goma_id
	end

	def caja
		Accessory.find self.caja_id
	end
	
end
