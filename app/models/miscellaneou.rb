class Miscellaneou < ActiveRecord::Base
	validates_presence_of :email_design, :email_ventas, :email_support, :phone_support, :iva, :notification_time

	def notification_hours
		if self.notification_time.blank?
			"18".to_i
		else
			self.notification_time.split(":").first.to_i
		end
	end

	def notification_minutes
		if self.notification_time.blank?
			"30".to_i
		else
			self.notification_time.split(":").second.to_i
		end
	end
end
