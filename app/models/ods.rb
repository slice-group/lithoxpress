class Ods < ActiveRecord::Base
  establish_connection :lithoxpress_database_development
	has_many :attachments
	has_many :validations
  has_one :approve_design
  accepts_nested_attributes_for :attachments


	def self.ods_json
	  Ods.where("Estatus != 8 and Anulado != true").order('OdsId DESC').as_json
  end

  def self.ods_client_json(cod)
		Ods.where(cliente: cod).order('OdsId DESC').as_json
  end

  def self.ods_design_json
	  json = Ods.where("Estatus" => 1, "AprobarModelo" => true, "Anulado" => false, "FechaEntrega" => nil).order('OdsId DESC').as_json
    json.each do |hash|
      user = Cliempre.find(hash[:cliente]).nombre
      hash.merge!({user_name: user, user_name_truncate: user.truncate(30)})
    end
    json
  end

  def as_json(options={})
    {
      odsid: self.OdsId,
      fecha: self.Fecha,
      estatus: self.Estatus,
      descripcion: self.Descripcion,
      description_truncate: self.Descripcion.truncate(40),
      cliente: self.Cliente,
      total: self.Total,
      preciounitario: self.PrecioUnitario,
      cantidad: self.Cantidad,
      fechaaprobar: self.FechaAprobar,
      abono: self.Abono,
      facturado: self.Facturado,
      fechaentrega: self.FechaEntrega == nil ? "N/A" : self.FechaEntrega,
      suspendida: self.Suspendida ? "Suspendida" : "No",
      anulado: self.Anulado ? "Anulada" : "No anulada",
      status: Stat.find(self.Estatus).name,
      aprobarmodelo: self.AprobarModelo,
      validation: "#{!self.validations.empty? ? self.validations.last.status : (self.Estatus == 1 and self.AprobarModelo?) ? 'Por aprobar' : 'N/A'}"
    }
  end

  #dashboard
  def self.status_design
  	Ods.where(status: 2)  		
  end

  def self.status_design_count(status)
  	count = 0;
  	Ods.where(estatus: 2).each do |order|
  		if !order.validations.empty? and order.validations.last.status == status
  			count += 1
  		end
  	end 
  	count
  end

  def self.group_by_status
  	status = { 1 => "pendiente", 2 => "diseño", 3 => "impreso", 4 => "numerado" , 5 =>"compaginado", 6 => "armada", 7 => "empaquetado"}
    rtrn = {}
    status.each do |key, value|
      rtrn[value] = Ods.where("Estatus = #{key}").count
    end
    rtrn
  end

  def self.en_proceso(user)
		Ods.where(["Estatus != ? and Cliente = ?", 8, user.cod]).count
	end

	def self.mis_ordenes_entregadas_por_dia(user)
		Ods.where(["Estatus = ? and Cliente = ?", 8, user.cod])
	end

  def user
    Cliempre.find(self.Cliente)  
  end

  def attachments_blank?
  	self.attachments.empty?
  end

end
