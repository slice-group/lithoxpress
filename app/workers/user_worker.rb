class UserWorker
	include Sidekiq::Worker
	include Sidetiq::Schedulable
	sidekiq_options retry: false

	recurrence { 
		daily.hour_of_day(Miscellaneou.first.notification_hours).minute_of_hour(Miscellaneou.first.notification_minutes) 
	}

	def perform(user)
		User.create_new_user_if_he_have_a_order(DateTime.now.to_s)
		User.send_notification_if_client_have_a_new_order(DateTime.now.to_s)
	end
end
