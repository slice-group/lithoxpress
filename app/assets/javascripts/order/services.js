angular.module('order').factory('orders', [
  '$http', function($http) {
    var orders = {};
    var route_index = '/orders/angular_index.json'
    var route_destroy = '/orders/delete/'


  orders.load = function() {
    Model.get(route_index, $http, function(output){
      orders.data = output;
    });
  };

  orders.destroy = function(ids, $scope) {
    Model.destroy(route_destroy, $http, $scope, ids, function(output){
      Model.get(route_index, $http, function(output){
        orders.data = output;
      });
    });
  };

  return orders;
  }
]);