angular.module('order', [])
	.controller('indexOrderCtrl', ['$scope','orders', function($scope, orders) {

		$scope.init = function(object, prices, action_name){
			$scope.accept = (action_name == "create")
			$scope.object = object
			$scope.cotizar = true

			for (var o in $scope.object) {
		    if ($scope.object.hasOwnProperty(o)) {
		    	if ($scope.object[o] == null) $scope.object[o] = "";
		    }
			}
		}	

		$scope.description = function(type) {
			$('.errors-message').css('display', 'none');
			switch(type) {
		    case 'type_armed':
		    	$('.title-manual').text("Tipo de armado")
		    	$('.description').find('.current').first().removeClass("current animate")
		    	$('#type-armed-description').addClass('current')
		    	setTimeout(function(){ $('#type-armed-description').addClass('animate'); }, 100);
		    	break;
		    case 'format':
		    	$('.title-manual').text("Tipo de formato")
		    	$('.description').find('.current').first().removeClass("current animate")
		    	$('#format-description').addClass('current')
		    	setTimeout(function(){ $('#format-description').addClass('animate'); }, 100);
		    	break;
		    case	'ink':
		    	$('.title-manual').text("Número de tintas de impresión")
		    	$('.description').find('.current').first().removeClass("current animate")
		    	$('#ink-description').addClass('current')
		    	setTimeout(function(){ $('#ink-description').addClass('animate'); }, 100);
		    	break
		    case 'material':
		    	$('.title-manual').text("Tipo de material")
		    	$('.description').find('.current').first().removeClass("current animate")
		    	$('#material-description').addClass('current')
		    	setTimeout(function(){ $('#material-description').addClass('animate'); }, 100);
		    	break
		    case 'copies':
		    	$('.title-manual').text("Número de copias")
		    	$('.description').find('.current').first().removeClass("current animate")
		    	$('#copies-description').addClass('current')
		    	setTimeout(function(){ $('#copies-description').addClass('animate'); }, 100);
		    	break
			}
		}

		$scope.new_cotization = function() {
			$("#manual").attr("style", "display: block !important")
			$("#result").css("display", "none")
			$(".errors-message").text("Ya puede escoger de nuevo sus opciones y realizar la cotización nuevamente.")
			$(".errors-message").css("color", "black")

			$(".form-inputs select").each(function(i, l) {
			  $(l).prop('disabled', false)
			})

			$("#search").prop('disabled', false)
			$(".btn-submit").prop('disabled', false)
			$("#new-cotization").css('display', 'none');
			$("#btn-cotization").css('display', 'block')
		}

		$scope.accept_term = function() {
			$scope.accept = true;


			if ($scope.accept) {
				$(".form-inputs select").each(function(i, l) {
				  $(l).prop('disabled', false)
				})
				$("#search").prop('disabled', false)
				$(".btn-submit").prop('disabled', false)
			}

			setTimeout(function(){
				$( "#order_type_armed" ).focus();
			}, 1);
		}

			
	}]);
