angular.module('reorder').factory('reorders', [
  '$http', function($http) {
    var reorders = {};
    var route_index = '/reorders/angular_index.json'
    var route_destroy = '/reorders/delete/'


  reorders.load = function() {
    Model.get(route_index, $http, function(output){
      reorders.data = output;
    });
  };

  reorders.destroy = function(ids, $scope) {
    Model.destroy(route_destroy, $http, $scope, ids, function(output){
      Model.get(route_index, $http, function(output){
        reorders.data = output;
      });
    });
  };

  return reorders;
  }
]);