angular.module('reorder', [])
  .controller('indexReorderCtrl', ['$scope','reorders', function($scope, reorders) {
 			reorders.load();
 			$scope.reorders = reorders;
			$scope.interval_a = 0;
			$scope.interval_b = 10;
			$scope.page = 1;
			$scope.btnDelete = false;
			//Alerts
			$scope.alert = false;
			$scope.msnAlert = "Exitosamente!";
			$scope.statusAlert="success";

			// routes path
			$scope.update_path = function (id) {
			  return '/reorders/' + id;
			};

			$scope.destroy_path = function (id) {
			  return '/reorders/' + id;
			};
			// --------------------

			$scope.init = function(){
				ctrl.pages = {};
			}

			$scope.destroy = function() {	
			 	if (confirm("¿Deseas eliminar las solicitudes seleccionadas?") == true) {
				 	reorders.destroy(ctrl.selected, $scope);
				 	ctrl.pageInit($scope);
 					$scope.reorders = reorders;
				}
			};

			$scope.selected = function(id){
				ctrl.itemSelected(id, $scope);
			};	

			$scope.allSelected = function(){
				ctrl.allItemsSelected($scope, $scope.reorders);
			};

			$scope.refresh = function() {
				reorders.load();
				$scope.reorders = reorders;				
				ctrl.pageInit($scope);
			};

			$scope.nextList = function(){
				ctrl.paginateControl($scope, $scope.reorders.data.length, "next");
			}

			$scope.lastList = function(){
				ctrl.paginateControl($scope, "last");			
			}

			$scope.alertClose = function(){
				$scope.alert = false;
			}

  }]);
