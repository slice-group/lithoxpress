angular.module('ods', [])
  .controller('indexOdsCtrl', ['$scope','ods', function($scope, orders) {
 			orders.load();
 			$scope.orders = orders;
			$scope.interval_a = 0;
			$scope.interval_b = 10;
			$scope.page = 1;
			$scope.btnDelete = false;
			//Alerts
			$scope.alert = false;
			$scope.msnAlert = "Exitosamente!";
			$scope.statusAlert="success";


			$scope.init = function(){
				ctrl.pages = {};
			}

			$scope.destroy = function() {	
			 	if (confirm("¿Deseas eliminar los mensajes seleccionados?") == true) {
				 	orders.destroy(ctrl.selected, $scope);
				 	ctrl.pageInit($scope);
 					$scope.orders = orders;
				}
			};

			$scope.selected = function(id){
				ctrl.itemSelected(id, $scope);
			};	

			$scope.allSelected = function(){
				ctrl.allItemsSelected($scope, $scope.orders);
			};

			$scope.refresh = function() {
				orders.load();
				$scope.orders = orders;				
				ctrl.pageInit($scope);
			};

			$scope.nextList = function(){
				ctrl.paginateControl($scope, $scope.orders.data.length, "next");
			}

			$scope.lastList = function(){
				ctrl.paginateControl($scope, "last");			
			}

			$scope.alertClose = function(){
				$scope.alert = false;
			}

  }]);
