angular.module('stamp').factory('stamps', [
  '$http', function($http) {
    var stamps = {};
    var route_index = '/admin/stamps/angular_index.json'
    var route_destroy = '/admin/stamps/delete/'


  stamps.load = function() {
    Model.get(route_index, $http, function(output){
      stamps.data = output;
    });
  };

  stamps.destroy = function(ids, $scope) {
    Model.destroy(route_destroy, $http, $scope, ids, function(output){
      Model.get(route_index, $http, function(output){
        stamps.data = output;
      });
    });
  };

  return stamps;
  }
]);