angular.module('stamp', [])
  .controller('indexStampCtrl', ['$scope','stamps', function($scope, stamps) {
 			stamps.load();
 			$scope.stamps = stamps;
			$scope.interval_a = 0;
			$scope.interval_b = 10;
			$scope.page = 1;
			$scope.btnDelete = false;
			//Alerts
			$scope.alert = false;
			$scope.msnAlert = "Exitosamente!";
			$scope.statusAlert="success";

			// routes path
			$scope.update_path = function (id) {
			  return '/admin/stamps/' + id;
			};

			$scope.destroy_path = function (id) {
			  return '/admin/stamps/' + id;
			};
			// --------------------

			$scope.init = function(){
				ctrl.pages = {};
			}

			$scope.destroy = function() {	
			 	if (confirm("¿Deseas eliminar las solicitudes seleccionadas?") == true) {
				 	stamps.destroy(ctrl.selected, $scope);
				 	ctrl.pageInit($scope);
 					$scope.stamps = stamps;
				}
			};

			$scope.selected = function(id){
				ctrl.itemSelected(id, $scope);
			};	

			$scope.allSelected = function(){
				ctrl.allItemsSelected($scope, $scope.stamps);
			};

			$scope.refresh = function() {
				stamps.load();
				$scope.stamps = stamps;				
				ctrl.pageInit($scope);
			};

			$scope.nextList = function(){
				ctrl.paginateControl($scope, $scope.stamps.data.length, "next");
			}

			$scope.lastList = function(){
				ctrl.paginateControl($scope, "last");			
			}

			$scope.alertClose = function(){
				$scope.alert = false;
			}

  }]);
