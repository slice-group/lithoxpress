var Model = {};

(function() {
    // Funcion para obtener json de Model
    // *routes* - ruta para el get
    // *$http* - angular.ajax
    // *output* - function para retornar objeto
    this.get = function (route, $http, output) {
      $http.get(route).success(function(data) {
        console.log('Successfully loaded');
        output(data);
      }).error(function() {
        console.error('Failed to load.');
      });
    }

    // Funcion para obtener json de Model
    // *routes* - ruta para el post
    // *$http* - angular.ajax
    // *ids* - arreglo con los ids de las objetos que se van a eliminar
    // *output* - function para retornar
    this.destroy = function(route, $http, $scope, ids, output) {
      $http({
        url: route, 
        method: "POST",
        params: { ids: ids.toString() }
      }).success(function(data) {
        console.log('Successfully destroy');
        $scope.alert = true;
        $scope.msnAlert = "los datos han sido eliminados exitosamente.";
        $scope.statusAlert="success";
        output(true);
      }).error(function() {
        $scope.alert = true;
        $scope.msnAlert = "los datos no pudieron ser eliminados.";
        $scope.statusAlert="danger";
        console.error('Failed to destroy.');
        output(false);
      });
    }

}).call(Model);