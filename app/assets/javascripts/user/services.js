angular.module('user').factory('users', [
  '$http', function($http) {
    var users = {};
    var route_index = '/admin/users/angular_index.json'
    var route_destroy = '/admin/users/delete/'


  users.load = function() {
    Model.get(route_index, $http, function(output){
      users.data = output;
    });
  };

  users.destroy = function(ids, $scope) {
    Model.destroy(route_destroy, $http, $scope, ids, function(output){
      Model.get(route_index, $http, function(output){
        users.data = output;
      });
    });
  };

  return users;
  }
]);