angular.module('user', [])
  .controller('indexUserCtrl', ['$scope','users', function($scope, users) {
 			users.load();
 			$scope.users = users;
			$scope.interval_a = 0;
			$scope.interval_b = 10;
			$scope.page = 1;
			$scope.btnDelete = false;
			$scope.roles = { "1": "Administrador", "2": "Ventas", "3": "Diseñador", "4": "Cliente", "5": "Suspendido" } 
			//Alerts
			$scope.alert = false;
			$scope.msnAlert = "Exitosamente!";
			$scope.statusAlert="success";

			// routes path
			$scope.update_path = function (id) {
			  return '/admin/users/' + id;
			};

			$scope.destroy_path = function (id) {
			  return '/admin/users/' + id;
			};
			// --------------------

			$scope.init = function(){
				ctrl.pages = {};
			}

			$scope.destroy = function() {	
			 	if (confirm("¿Deseas eliminar los mensajes seleccionados?") == true) {
				 	users.destroy(ctrl.selected, $scope);
				 	ctrl.pageInit($scope);
 					$scope.users = users;
				}
			};

			$scope.selected = function(id){
				ctrl.itemSelected(id, $scope);
			};	

			$scope.allSelected = function(){
				ctrl.allItemsSelected($scope, $scope.users);
			};

			$scope.refresh = function() {
				users.load();
				$scope.users = users;				
				ctrl.pageInit($scope);
			};

			$scope.nextList = function(){
				ctrl.paginateControl($scope, $scope.users.data.length, "next");
			}

			$scope.lastList = function(){
				ctrl.paginateControl($scope, "last");			
			}

			$scope.alertClose = function(){
				$scope.alert = false;
			}

  }]);

  

