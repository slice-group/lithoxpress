class UserMailer < ActionMailer::Base
  default from: "noreply@suimprenta.com.ve"

  def mail_registration(client, password)
    @client = client
    @password = password
    mail(to: @client.email, subject: client.name+' gracias por registrarse en lithoxpress online.')
  end
end
