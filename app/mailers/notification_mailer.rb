class NotificationMailer < ActionMailer::Base
  

  #mailer para cuando se le cambia de status a una cotización
  def status_reorder(reorder)
  	@reorder = reorder
  	mail(from: "ventas@suimprenta.com.ve", to: @reorder.user.email, subject: @reorder.user.name+' Su cotización ha sido procesada.')
  end

  #mailer para cuando el cliente cotiza una orden
  def client_reorder(reorder)
  	@reorder = reorder
  	mail(from: "noreply@suimprenta.com.ve", to: Miscellaneou.first.email_ventas, subject: @reorder.user.name+' ha recotizado una orden.')
  end

  #mailer para cuando de sube un diseño.
  def upload_design(order)
  	@order = order
  	mail(from: "arte@suimprenta.com.ve", to: @order.user.email, subject: @order.user.nombre+' se han subido diseños a su orden.')
  end

  #mailer para cuando el cliente valida un diseño.
  def status_validation(order, validation)
  	@order = order
  	@validation = validation
  	mail(from: "noreply@suimprenta.com.ve", to: Miscellaneou.first.email_design, subject: @order.user.nombre+" ha validado los diseños de la orden N° #{@order.OdsId}")
  end

  #mailer para cuando se le crea una ods nueva al cliente.
  def ods_new(order, client)
    @order = order
    @client = client
    mail(from: "noreply@suimprenta.com.ve", to: client.email, subject: client.nombre+' se ha creado una orden nueva.')
  end
end
