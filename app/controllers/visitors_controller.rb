class VisitorsController < ApplicationController
	def index
		Location.add(request.remote_ip)
	end

	def catalog
    @categories = Category.all
    @category = Category.find(params[:id])
    @stamps = @category.complement_stamps.order(:group).page(params[:page]).per(5)
  end

  def modal
  	@stamp = ComplementStamp.find(params[:stamp_id])
  end

end
