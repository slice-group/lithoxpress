class OdsController < ApplicationController
	before_filter :authenticate_user!
  layout 'admin/application'

  
	def index
	end

	def angular_index
    if current_user.has_role? :cliente
      orders = Ods.ods_client_json(current_user.cod)
    elsif current_user.has_role? :diseñador
      orders =  Ods.ods_design_json
    else 
      orders =  Ods.ods_json
    end
    respond_to do |format|
      format.html
      format.json { render :json => orders  }
    end
  end

  def show
    @validation = Validation.new
    @order = Ods.find(params[:id])
    @order.validations.empty? ? @valid = nil : @valid = @order.validations.last.status 
    if current_user.has_role? :cliente
      authorize_client(@order)
    elsif current_user.has_role? :diseñador
      authorize_design(@order)
    end    
  end

  def edit
    @ods = Ods.find(params[:id])
    permit_edit(@ods) #validar que la orden este en estatus de diseño

    if @ods.validations.empty? 
      build_attachments(@ods)
    elsif @ods.validations.last.status != "Aprobado"
      build_attachments(@ods)
    else @ods.validations.last.status == "Aprobado"
      redirect_to od_path(@ods.OdsId), flash: { danger: "No puede gestionar bocetos, el diseño ya ha sido aprobado por el cliente." }
    end
  end

  def update
    @ods = Ods.find(params[:id])
    permit_edit(@ods)
      #approve
      if @ods.approve_design.nil?
        ApproveDesign.create(approve: params[:approve_design].nil? ? false : true, ods_id: @ods.id)
      else
        approve_design = ApproveDesign.find_by_ods_id(@ods.id)
        approve_design.update_attributes(approve: params[:approve_design].nil? ? false : true )
      end


      params[:ods][:attachments_attributes].each do |key, value|
        if value[:image]
          @attachment = @ods.attachments.create(:image => value[:image], description: value[:description], :ods_id => @ods.OdsId)

          unless @attachment.errors.empty?
            return redirect_to edit_od_path(@ods.OdsId), flash: { danger: @attachment.errors.messages }        
          end
        elsif !value[:description].blank?
          @attachment = @ods.attachments.create(:image => value[:image], description: value[:description], :ods_id => @ods.OdsId)
          unless @attachment.errors.empty?
            return redirect_to edit_od_path(@ods.OdsId), flash: { danger: @attachment.errors.messages }        
          end
        end

      end

      if @attachment
        NotificationMailer.upload_design(@ods).deliver #send email
      end
      redirect_to od_path(@ods.OdsId)
  end

  def clone
    ods = Ods.find(params[:id])
    authorize_clone(current_user, ods)
    clone = Reorder.new(ods_id: ods.OdsId, status: 'Por cotizar', cantidad: ods.Cantidad, user_id: current_user.id, price: 0, emision_date: 0)
    if ods.Estatus == 8
      clone.save
      NotificationMailer.client_reorder(clone).deliver
      redirect_to reorder_path(clone), notice: "La orden ha sido cotizada satisfactoriamente"
    else
      redirect_to orders_path, flash: { danger: "Solo puedes cotizar pedidos en estado Completados" }
    end
  end

	private

  def ods_params
    params.require(:ods).permit(:odsid, :fecha, :cliente, :estatus, :total, :abono, :articuloid, :preciounitario, :cantidad, :iva, :formato, :material, :numerojuegos, :numerotintasfront, :tintasfront, :amboslados, :numerotintasback, :tintasback, :notas, :modelo, :aprobarmodelo, :fechaaprobar, :descripcion, :numdesde, :numhasta, :controldesde, :controlhasta, :numerocopias, :diasentrega, :color1, :color2, :color3, :color4, :color5, :perf1, :perf2, :perf3, :perf4, :perf5, :cambio1, :cambio2, :cambio3, :cambio4, :cambio5, :contacto, :telefonos, :concepto, :contribuyente, :facturado, :efectivo, :cheque, :tcredito, :tdebito, :anulado, :fechahora, :fechaentrega, :para, :orientacion, :entregado, :ubicacion, :usuario, :parent_odsid, :suspendida, :diasrestantes, :approve_design, attachments_attributes: [:id, :ods_id, :image, :description])
  end

  def authorize_client(order)
    if order.Cliente != current_user.cod.delete(" ")
      authorize! :edit, @user, :message => "No estas autorizado para ver esta orden."
    end
  end

  def authorize_design(order)
    if order.Estatus != 1
      authorize! :show, @user, :message => "No estas autorizado para ver esta orden."
    end
  end

  def authorize_clone(user, order)
    unless user.has_role? :cliente and order.Cliente == user.cod.delete(" ")
      raise CanCan::AccessDenied.new("No tienes permisos para cotizar este pedido", :new, Order)
    end
  end

  def permit_edit(order)
    if order.Estatus != 1 or order.AprobarModelo == false
      raise CanCan::AccessDenied.new("Solo puedes subir diseños a ordenes que esten estado pendiente.", :edit, Ods)
    end

    unless current_user.has_role? :diseñador
      raise CanCan::AccessDenied.new("Solo los diseñadores tienen acceso.", :edit, Ods)
    end
  end

  def build_attachments(order)
    limit = 3-order.attachments.count

    limit.times do |i|
      @ods_attachment = order.attachments.build
    end
  end

end
