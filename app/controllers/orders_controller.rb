class OrdersController < ApplicationController
  layout 'application'

  respond_to :html

  def new
    @order = Order.new
    respond_with(@order)
  end

  def create
    @prices_count = SecureRandom.hex(4)
    if params[:search]
      query = params[:search].blank? ? "xxxxxxxx" : params[:search]
      @order = Order.new
      @iva = Miscellaneou.first.iva/100
      @prices = Price.where("Codigo LIKE ? ", "%#{query}%").order("Precio ASC")
      respond_to do |format|
        format.js { }
      end
    else
      @order = Order.new(order_params)
      respond_to do |format|
        if @order.valid?
          @iva = Miscellaneou.first.iva/100
          @prices = Price.where("Codigo LIKE ? ", "%#{@order.create_cod}%").order("Precio ASC")
          format.js { }
        else
          format.html { render action: 'new', order: @order }
        end
      end
    end
  end
  
  private
    def set_order
      @order = Order.find(params[:id])
    end

    def order_params
      params.require(:order).permit(:type_armed, :format, :ink, :material, :copies)
    end

end
