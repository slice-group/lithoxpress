class UsersController < ApplicationController
  before_filter :authenticate_user!, except: [:sign_up, :register]
  layout 'admin/application'


  def index
   authorize
   @users = User.all
  end

  def sign_up
    redirect_to root_path if current_user
  end

  def register
    cliempre = User.cliempre(params[:email])
    respond_to do |format|
      if cliempre.blank? or params[:email].blank?       
        format.html { redirect_to sign_up_user_path, flash: { danger: "El correo electronico no se encuentra, debe ingresar su correo electronico principal registrado en nuestro sistema, para mas informacion llamar al (0269)-247.92.16" } }
      else
        password = SecureRandom.hex(4);
        user = User.new name: cliempre.nombre, email: cliempre.email, password: password, password_confirmation: password, role_ids: "4", cod: cliempre.codigo, rif: cliempre.nrorif, phone: cliempre.telefonos
        if user.save
          UserMailer.mail_registration(User.find_by_email(cliempre.email), password).deliver
          format.html { redirect_to new_user_session_path, notice: "El usuario ha sido registrado, diríjase a su correo para verificar sus datos." }
        else
          format.html { redirect_to sign_up_user_path, flash: { danger: "El cliente ya esta registrado" } }
        end
      end
    end
  end

  def angular_index
    users = User.user_json(current_user)
    respond_to do |format|
      format.html
      format.json { render :json => users  }
    end
  end

  def new
    authorize
    @user = User.new
  end

  def show
    @user = User.find(params[:id])
    unless current_user.has_role? :admin or current_user.has_role? :ventas
      redirect_to :back, :alert => t("notification.denied")
    end
  end

  def edit
    @user = User.find(params[:id])
    authorize_edit @user, current_user
  end
  
  def update
    @user = User.find(params[:id])

    if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
      params[:user].delete(:password)
      params[:user].delete(:password_confirmation)
    end

    respond_to do |format|
      if @user.update_attributes(user_params)
        format.html { redirect_to users_path, :notice => t("notification.update") }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def create
    @user = User.new(user_params)
    respond_to do |format|
      if @user.save
        @user.add_role Role.find(user_params[:role_ids]).name
        format.html { redirect_to users_path, notice: t("notification.create") }
        format.json { render action: 'show', status: :created, location: @user }
      else
        format.html { render action: 'new' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    authorize! :destroy, @user, :message => "#{t('cancan.messages.access_denied')} #{t('models.user')}"
    user = User.find(params[:id])
    unless user == current_user
      user.destroy
      redirect_to users_path, :notice => t("notification.delete")
    else
      redirect_to users_path, :notice => t("notification.un_delete")
    end
  end
    
  def delete
    authorize! :destroy, @user, :message => "#{t('cancan.messages.access_denied')} #{t('models.user')}"
    User.destroy( redefine_destroy params[:ids].split(",") )
    respond_to do |format|
      format.html { render nothing: true }
      format.json { redirect_to users_path, notice: t("notification.delete") }
    end
  end

  private

  #metodo para redefinir el array de los elementos selecionados que se van a eliminar
  def redefine_destroy(params)
    params.sort.each do |id|
      params.delete id unless User.exists? id
    end
  end

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation, :role_ids, :encrypted_password)
  end

  def authorize_edit(user, current_user)
    if user.id != current_user.id and (current_user.has_role? :cliente or current_user.has_role? :ventas or current_user.has_role? :diseñador)
      raise CanCan::AccessDenied.new("No tienes permisos para editar perfiles.", :edit, User)
    end
  end

  def authorize
    unless current_user.has_role? :admin
       raise CanCan::AccessDenied.new("No tienes permisos gestionar el módulo usuarios", :new, User)
    end
  end

end
