class ReordersController < ApplicationController
  before_action :set_reorder, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  layout 'admin/application'
  
  respond_to :html

  def index
    authorize_design(current_user)
    if current_user.has_role? :cliente
      @reorders = Reorder.where(user_id: current_user.id).order('id DESC')
    elsif current_user.has_role? :admin or current_user.has_role? :ventas
      @reorders = Reorder.all.reverse
    end
  end

  def angular_index
    authorize_design(current_user)
    if current_user.has_role? :cliente
      @reorders = Reorder.where(user_id: current_user.id).order('id DESC')
    elsif current_user.has_role? :admin or current_user.has_role? :ventas
      @reorders = Reorder.all.reverse
    end
    respond_to do |format|
      format.html
      format.json { render :json => @reorders  }
    end
  end

  def show
    @order = Ods.find(@reorder.ods_id)
    total = @reorder.price*@reorder.cantidad
    iva = Miscellaneou.first.iva/100
    @total_amount = total+(total*iva)
    respond_with(@reorder)
  end

  def new
    @reorder = Reorder.new
    respond_with(@reorder)
  end

  def edit
    authorize_user current_user
    validate_aprobation @reorder
  end

  def create
    @reorder = Reorder.new(reorder_params)
    @reorder.save
    respond_with(@reorder)
  end

  def update
    authorize_user current_user
    validate_aprobation @reorder
    @reorder.update(reorder_params)
    respond_with(@reorder)
  end

  def changed_status
    reorder = Reorder.find(params[:id])
    authorize_changed_status(current_user)
    
    
    if params[:stat] == '1' and reorder.status == "Por cotizar" and reorder.price > 0 and reorder.emision_date > 0
      reorder.update_attribute(:status, "Cotizada")
      NotificationMailer.status_reorder(reorder).deliver #send email
      redirect_to reorder_path(reorder.id), notice: "Cotización aprobada"
    elsif params[:stat] == '1' or reorder.price == 0 or reorder.emision_date == 0
      if reorder.price == 0
        return redirect_to reorder_path(reorder.id), flash: { danger: "La cotización debe tener un precio para poder ser cotizada." }
      elsif reorder.emision_date == 0
        return redirect_to reorder_path(reorder.id), flash: { danger: "La cotización debe tener los días de emision para poder ser cotizada" }
      end
    end

    if params[:stat] == '0' and reorder.status != "Por cotizar"
       authorize_undo_status(current_user)
       reorder.update_attribute(:status, "Por cotizar")
       redirect_to reorder_path(reorder.id), notice: "La cotización ha cambiado al estatus 'Por cotizar'"
    elsif params[:stat] == '2' and reorder.status == "Por cotizar"
        reorder.update_attribute(:status, "No cotizada")
        NotificationMailer.status_reorder(reorder).deliver #send email
        redirect_to reorder_path(reorder.id), notice: "Cotización desaprobada"
    elsif params[:stat] == '2' and reorder.status == "Cotizada"
        redirect_to reorder_path(reorder.id), flash: { danger: "Esta cotización ya ha sido aprobada, no se puede desaprobar." }
    end 
    
  end

  def destroy
    authorize_user current_user
    @reorder.destroy
    respond_with(@reorder)
  end

  def delete
    authorize_user current_user
    Reorder.destroy( redefine_destroy params[:ids].split(",") )
    respond_to do |format|
      format.html { render nothing: true }
    end
  end

  private
    def redefine_destroy(params)
      params.sort.each do |id|
        params.delete id unless Order.exists? id
      end
    end

    def set_reorder
      @reorder = Reorder.find(params[:id])
    end

    def reorder_params
      params.require(:reorder).permit(:cantidad, :stat, :price, :emision_date, :description)
    end

    #validations
    def authorize_design(user)
      if user.has_role? :diseñador
        raise CanCan::AccessDenied.new("No estas autorizado para gestionar el módulo de ordenes.", :show, Reorder)
      end
    end

    def authorize_user(user)
      unless !user.has_role? :diseñador
        raise CanCan::AccessDenied.new("Solo los clientes tienen acceso a este procedimiento.", :show, Reorder)
      end
    end

    def authorize_changed_status(user)
      unless user.has_role? :ventas or user.has_role? :admin
        raise CanCan::AccessDenied.new("No tiene acceso a este procedimiento.", :edit, Reorder)
      end
    end

    def authorize_undo_status(user)
      if !user.has_role? :admin
        raise CanCan::AccessDenied.new("Solo los administradores pueden deshacer los estatus.", :new, Order)
      end
    end

    def validate_aprobation(reorder)
      if reorder.status == "Cotizada"
        raise CanCan::AccessDenied.new("La orden ya ha sido cotizada, no puede editarla.", :new, Order)
      end
    end
end
