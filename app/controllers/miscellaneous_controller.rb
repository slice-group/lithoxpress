class MiscellaneousController < ApplicationController
  before_action :set_miscellaneou, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource

  respond_to :html

  def index
    @miscellaneous = Miscellaneou.all
    respond_with(@miscellaneous)
  end

  def show
    respond_with(@miscellaneou)
  end

  def new
    @miscellaneou = Miscellaneou.new
    respond_with(@miscellaneou)
  end

  def edit
  end

  def create
    @miscellaneou = Miscellaneou.new(miscellaneou_params)
    @miscellaneou.save
    respond_with(@miscellaneou)
    reset_sidekiq
  end

  def update
    @miscellaneou.update(miscellaneou_params)
    if @miscellaneou.errors.blank?
      reset_sidekiq
      return redirect_to edit_miscellaneou_path(params[:id]),  flash: { notice: "Guardado con exito." }
    else
      respond_with(@miscellaneou)
    end
  end

  def destroy
    @miscellaneou.destroy
    respond_with(@miscellaneou)
  end

  def send_notifications_manual
    User.create_new_user_if_he_have_a_order(params[:date])
    User.send_notification_if_client_have_a_new_order(params[:date])
    redirect_to edit_miscellaneou_path(params[:miscellaneou_id]),  flash: { notice: "Correos enviados con exito." }
  end

  private
    def set_miscellaneou
      @miscellaneou = Miscellaneou.find(params[:id])
    end

    def miscellaneou_params
      params.require(:miscellaneou).permit(:email_design, :email_ventas, :iva, :email_support, :phone_support, :notification_time)
    end

    def reset_sidekiq
      system("bundle exec sidekiq -d -L log/sidekiq.log -C config/sidekiq.yml -e production")
    end
end
