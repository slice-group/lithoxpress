class StampsController < ApplicationController
	before_filter :authenticate_user!
  load_and_authorize_resource
  layout :resolve_layout

	def index
	end

	def angular_index
    stamps = Stamp.all_stamps.order("codigo ASC")
    respond_to do |format|
      format.html
      format.json { render :json => stamps  }
    end
  end

  def show
  	@stamp = Stamp.find(params[:id])
  end

  def edit
  	@complement = ComplementStamp.find_by_stamp_id(params[:id])
  	@complement = ComplementStamp.new unless @complement
  end

  def update
  	@complement = ComplementStamp.find_by_stamp_id(params[:id])

  	#update
  	if @complement
  		respond_to do |format|
	      if @complement.update_attributes(complement_stamp_params)
	        format.html { redirect_to stamp_path, notice: "Sello actualizado satisfactoriamente" }
	      else
	        format.html { render action: 'edit' }
	        format.json { render json: @complement.errors, status: :unprocessable_entity }
	      end
	    end
	  #create
  	else
  		@complement = ComplementStamp.new(complement_stamp_params)
  		respond_to do |format|
	      if @complement.save
	        format.html { redirect_to stamp_path, notice: "Sello creado satisfactoriamente" }
	      else
	        format.html { render action: 'edit' }
	        format.json { render json: @complement.errors, status: :unprocessable_entity }
	      end
	    end
  	end
  end

  private

  def complement_stamp_params
    params.require(:complement_stamp).permit(:image_stamp, :example_stamp, :height, :width, :public, :stamp_id, :category_id, :group, :almohadilla_id, :caja_id, :goma_id)
  end


  def resolve_layout
    case action_name
      when "catalog", "catalog_show"
        "application"
      else 
        "admin/application"
    end
  end

end
