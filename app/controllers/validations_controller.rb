class ValidationsController < ApplicationController
  before_action :set_validation, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  layout 'admin/application'

  respond_to :html

  def index
    @validations = Validation.all
    respond_with(@validations)
  end

  def show
    respond_with(@validation)
  end

  def new
    
  end

  def edit
  end

  def create
    permit_validate(Ods.find(params[:id]))
    @validation = Validation.new(validation_params)
    ods = Ods.find(params[:id])
    @validation.ods_id = ods.id
    if permit_client?(current_user, params[:id])
      respond_to do |format|
        if @validation.save
          NotificationMailer.status_validation(ods, @validation).deliver #send email
          format.html { redirect_to od_path ods.id, anchor: "validation" }
        else
          format.html { redirect_to od_path(ods.id), flash: { danger: "Debe llenar todos los campos en la validación." } }
        end
      end
    else
      raise CanCan::AccessDenied.new("No estas autorizado para validar esta orden.", :new, Validation)
    end
  end

  def update
    permit_validate(validation_params[:ods_id])
    @validation.update(validation_params)
    respond_with(@validation)
  end

  def destroy
    @validation.destroy
    respond_with(@validation)
  end

  private
    def set_validation
      @validation = Validation.find(params[:id])
    end

    def validation_params
      params.require(:validation).permit(:status, :description, :active, :ods_id, :selection_image)
    end

    def permit_client?(user, id)
      order = Ods.find(id)
      if user.has_role? :cliente and order.Cliente == user.cod.delete(" ")
        true
      else 
        false
      end
    end

    def permit_validate(order)
      if order.attachments.empty?
        raise CanCan::AccessDenied.new("No hay bocetos que validar.", :edit, Ods)
      elsif !order.validations.empty? and order.validations.last.status == "Aprobado"
        raise CanCan::AccessDenied.new("Ya la orden fue aprobada por usted, no puede volver a validar.", :edit, Ods)
      end
    end
end
