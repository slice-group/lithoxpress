class AttachmentsController < ApplicationController
  before_action :set_attachment, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  layout 'admin/application'

  respond_to :html

  def index
    @attachments = Attachment.all
    respond_with(@attachments)
  end

  def show
    respond_with(@attachment)
  end

  def new
    @attachment = Attachment.new
    respond_with(@attachment)
  end

  def edit
  end

  def create
    @attachment = Attachment.new(attachment_params)
    @attachment.save
    respond_with(@attachment)
  end

  def update
    @attachment.update(attachment_params)
    NotificationMailer.upload_design(Ods.find(@attachment.ods_id)).deliver #send email
    redirect_to edit_od_path(attachment_params[:ods_id])
  end

  def destroy
    @attachment.destroy
    respond_to do |format|
      format.html { redirect_to edit_od_path(@attachment.ods_id), notice: t("notification.delete") }
    end
  end

  private
    def set_attachment
      @attachment = Attachment.find(params[:id])
    end

    def attachment_params
      params.require(:attachment).permit(:ods_id, :image, :choose_model, :description)
    end
end
