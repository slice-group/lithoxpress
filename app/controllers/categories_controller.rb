class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  layout 'admin/application'
  respond_to :html
  load_and_authorize_resource :except => [:create]

  def index
    @categories = Category.all
    respond_with(@categories)
  end

  def angular_index
    categories = Category.all
    respond_to do |format|
      format.html
      format.json { render :json => categories  }
    end
  end

  def show
    respond_with(@category)
  end

  def new
    @category = Category.new
    respond_with(@category)
  end

  def edit
  end

  def create
    @category = Category.new(category_params)
    @category.save
    respond_with(@category)
  end

  def update
    @category.update(category_params)
    respond_with(@category)
  end

  def destroy
    @category.destroy
    respond_with(@category)
  end

  def delete
    Category.destroy( redefine_destroy params[:ids].split(",") )
    respond_to do |format|
      format.html { render nothing: true }
      format.json { redirect_to categories_path, notice: t("notification.delete") }
    end
  end

  private

  #metodo para redefinir el array de los elementos selecionados que se van a eliminar
  def redefine_destroy(params)
    params.sort.each do |id|
      params.delete id unless User.exists? id
    end
  end

    def set_category
      @category = Category.find(params[:id])
    end

    def category_params
      params.require(:category).permit(:name, :description)
    end
end
