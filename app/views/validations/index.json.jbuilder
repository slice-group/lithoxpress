json.array!(@validations) do |validation|
  json.extract! validation, :id, :status, :description, :active, :ods_id
  json.url validation_url(validation, format: :json)
end
