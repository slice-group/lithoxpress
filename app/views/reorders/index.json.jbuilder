json.array!(@reorders) do |reorder|
  json.extract! reorder, :id, :ods_id, :status, :cantidad
  json.url reorder_url(reorder, format: :json)
end
