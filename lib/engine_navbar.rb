module Navbar

	class << self
    mattr_accessor :routes_admin, :routes_front
    # add default values of more config vars here
  	end

	# this function maps the vars from your app into your engine
	def self.setup(&block)
	  yield self
	end

end